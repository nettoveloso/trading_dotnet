using System;
using System.Linq;
using trading.Core.Interfaces;
using trading.Core.Entities;
using trading.Infrastructure.Data;
using Microsoft.EntityFrameworkCore;
using System.Text;
using trading.Infrastructure.Utils;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using trading.Infrastructure.Settings;
using System.Security.Cryptography;
using System.Globalization;

namespace trading.Infrastructure.Services
{
    public class UserManagementService : IUserManagementService
    {
        private readonly AppDbContext _context;

        private readonly JwtSettings _settings;

        public UserManagementService(AppDbContext context, JwtSettings settings){
            _context    = context;
            _settings   = settings;
        }

        public bool IsValidUser(string email , string password)
        {
            var usuario = _context.Usuario.AsQueryable().Where(x=> x.Email.Equals(email) ).FirstOrDefault();
            //Usuario usuario = _context.Usuario.First(user => user.Email.Equals(email));

            if(usuario != null)
            {
                return SecurityUtil.VerifyMd5Hash( password, usuario.Password) ;
            }else{
                var teste = usuario;
            }

            return false;
        }

        public string GetToken(){

            var secretKey           = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_settings.SecretString));
            var signinCredentials   = new SigningCredentials(secretKey, SecurityAlgorithms.HmacSha256);
    
            var tokeOptions = new JwtSecurityToken(
                issuer: "http://localhost:5000",
                audience: "http://localhost:5000",
                expires: DateTime.Now.AddMinutes(5),
                signingCredentials: signinCredentials
            );
    
            var tokenString = new JwtSecurityTokenHandler().WriteToken(tokeOptions);
            return tokenString;
            
        }

        public object GetUsuario(string email, string password){
            var usuario = _context.Usuario.AsQueryable().Where(x=> x.Email.Equals(email) ).FirstOrDefault();
            return usuario;
        }

        public int Create(Usuario usuario)
        {
            var _consulta = _context.Usuario.AsQueryable().Where(x=> x.Email.Equals(usuario.Email) ).FirstOrDefault();
            // Verificar se o e-mail já está cadastrado no banco
            if(_consulta == null)
            {
                Usuario newUsuario   = new Usuario();
                // Encriptar a senha
                newUsuario.Email         = usuario.Email;
                newUsuario.Name          = usuario.Name;
                newUsuario.Password      = SecurityUtil.GetMd5Hash(MD5.Create(), usuario.Password);
                newUsuario.Data_cadastro     = DateTime.Now;
                newUsuario.Data_ultimo_login = DateTime.Now;
                _context.Add(newUsuario);
                return _context.SaveChanges();
            }else{
                return -1;
            }
        }
    }
}
        