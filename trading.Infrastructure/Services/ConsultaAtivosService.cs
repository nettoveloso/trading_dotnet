using System.Collections.Generic;
using System.Collections;
using System;
using System.Data;
using trading.Core.Interfaces;
using RestSharp;
using RestSharp.Authenticators;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Serialization;
using System.Json;
using trading.Core.Struct;

namespace trading.Infrastructure.Services
{
    public class ConsultaAtivosService : ConsultaAtivoInterface
    {

        public object BuscaAtivo(string ativo){
            var client = new RestClient("https://www.alphavantage.co/");
            // client.Authenticator = new HttpBasicAuthenticator(username, password);

            var request = new RestRequest("query", Method.GET);
            //request.AddParameter("function", "TIME_SERIES_INTRADAY"); // adds to POST or URL querystring based on Method
            request.AddParameter("function", "SYMBOL_SEARCH"); // adds to POST or URL querystring based on Method
            request.AddParameter("keywords", ativo); 
            //request.AddParameter("interval", "60min"); 
            //request.AddParameter("outputsize", "compact"); 
            request.AddParameter("apikey", "FUNZOY5KPNJFBE5Z"); 
            
            List<Ativo> acao = new List<Ativo>();
            
            IRestResponse response = client.Execute(request);
            var content = response.Content;

            DataSet dataSet = JsonConvert.DeserializeObject<DataSet>(content);
            DataTable dataTable = dataSet.Tables["bestMatches"];
            List<String> acoes = new List<String>();
            foreach (DataRow row in dataTable.Rows)
            {
                acoes.Add(row["1. symbol"].ToString());
                //a.Symbol    = row["1. symbol"].ToString();
                //a.Name      = row["2. name"].ToString();
                //acao.Add(a);
            }

            var resultado = new { 
                status = response.StatusCode,
                content = acoes,
                ErrorMessage = response.ErrorMessage
            };

            return resultado;
        }

        public object CotacaoHoje(string ativo){
             
            var client = new RestClient("https://www.alphavantage.co/");
            // client.Authenticator = new HttpBasicAuthenticator(username, password);

            var request = new RestRequest("query", Method.GET);
            //request.AddParameter("function", "TIME_SERIES_INTRADAY"); // adds to POST or URL querystring based on Method
            request.AddParameter("function", "GLOBAL_QUOTE"); // adds to POST or URL querystring based on Method
            request.AddParameter("symbol", ativo); 
            //request.AddParameter("interval", "60min"); 
            //request.AddParameter("outputsize", "compact"); 
            request.AddParameter("apikey", "FUNZOY5KPNJFBE5Z"); 
            
            IRestResponse response = client.Execute(request);
            var content = response.Content;

            JObject o   = JObject.Parse(response.Content);
            JArray a    = (JArray)o["bestMatches"];

            List<Ativo> acao = a.ToObject<List<Ativo>>();

            var resultado = new { 
                status = response.StatusCode,
                content = acao,
                ErrorMessage = response.ErrorMessage
            };           

            // easy async support
            /* client.ExecuteAsync(request, resposta => 
            {
                Console.WriteLine(content);
            });*/

            return resultado;
            
        }
    }
}