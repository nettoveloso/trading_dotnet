using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using trading.Core.Entities;

namespace trading.Infrastructure.Data
{
    public class AppDbContext : IdentityDbContext
    {

        public DbSet<Usuario> Usuario {get; set;}

        public DbSet<UsuarioAtivo> UsuarioAtivos {get; set;}

        public AppDbContext(DbContextOptions<AppDbContext> options): base(options)
        {
            
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
        }
    }
}