using trading.Core.Entities;

namespace trading.Core.Interfaces
{
    public interface IUserManagementService
    {
        bool IsValidUser(string username, string password);

        string GetToken();

        int Create(Usuario usuario);

        object GetUsuario(string email, string password);
    }
}