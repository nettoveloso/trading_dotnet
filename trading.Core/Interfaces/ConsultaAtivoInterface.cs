namespace trading.Core.Interfaces
{  
    public interface ConsultaAtivoInterface
    {
        object CotacaoHoje(string ativo);

        object BuscaAtivo(string ativo);
    }    
}