using trading.Core.Entities;

namespace trading.Core.Interfaces
{
    public interface IAuthenticateService
    {
        bool IsAuthenticated(LoginModel request, out string token);
    }
}