namespace trading.Core.Struct
{
    public struct AtivoValue
    {
        public string Symbol;
        public double Name;
        public double High;
        public double Low;
        public double Price;
        public int Volume;
        public string Latest_trading_day;
        public double Pevious_close;
        public double Change;
        public double Change_percent;
    }
}