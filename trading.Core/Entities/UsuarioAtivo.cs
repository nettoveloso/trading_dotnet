using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace trading.Core.Entities
{
    [Table("user_ativo")]
    public class UsuarioAtivo
    {
         
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key,Column("id", Order = 0)]
        public int Id {get; set;}

        [Column("ativo")]
        //[Required]
        public string Ativo {get; set;}

        [Column("dt_compra")]
        public DateTime Dt_compra {get; set;}

        [Column("vlr_compra")]
        //[Required]
        public double Vlr_compra {get; set;}

        [Column("vlr_atual")]
        //[Required]
        public double Vlr_atual {get; set;}
        
    }
}