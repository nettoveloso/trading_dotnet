using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace trading.Core.Entities
{
    [Table("users")]
    public class Usuario
    {   
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key,Column("id", Order = 0)]
        public int Id {get; set;}

        [Column("name")]
        //[Required]
        public string Name {get; set;}

        [Column("email")]
        [EmailAddress,Required]
        public string Email {get; set;}

        [Column("password")]
        [Required]
        public string Password {get; set;}
        
        [Column("updated_at")]
        public DateTime Data_ultimo_login {get; set;}

        [Column("created_at")]
        public DateTime Data_cadastro {get; set;}

    }
}