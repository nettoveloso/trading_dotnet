using Microsoft.AspNetCore.Mvc;
using trading.Core.Entities;
using trading.Core.Interfaces;
using System;

namespace trading.Web.Controllers
{
    [Route("api/v1/auth")]
    [ApiController]
    public class AuthController : ControllerBase
    {
        private readonly IUserManagementService _service;

        public AuthController(IUserManagementService service){
            _service = service;
        }

        // GET api/values
        [HttpPost, Route("login")]
        public IActionResult Login([FromBody]Usuario user)
        {
            try
            {
                if (user == null)
                {
                    return BadRequest("Paramentros invalidos");
                }

                if( _service.IsValidUser(user.Email ,user.Password))
                {
                    var retorno = new {
                        Token = _service.GetToken(),
                        user  = _service.GetUsuario(user.Email ,user.Password)
                    };

                    return Ok(retorno);
                }
                else
                {
                    return Unauthorized();
                }
            }catch(Exception e){
                    // Não existe nenhum usuário com este e-mail
                    Response.StatusCode = 401; // Não autorizado            
                    return new ObjectResult(e.Message);
            }   
        }
    }
}