using Microsoft.AspNetCore.Mvc;
using trading.Core.Entities;
using trading.Core.Interfaces;
using System;

namespace trading.Web.Controllers
{
    [Route("api/v1/usuario")]
    [ApiController]
    public class UsuarioController : Controller
    {
         private readonly IUserManagementService _service;

        public UsuarioController(IUserManagementService service){
            _service = service;
        }

          //api/v1/usuarios/registro
        [HttpPost,  Route("novo")]
        public IActionResult Registro([FromBody] Usuario usuario){
            try
            {
                if(_service.Create(usuario) > 0)
                {
                    return Ok(new{status = 200,msg="Usuário cadastrado com sucesso"});
                }else{
                    return Ok(new{status = 500,msg="E-mail já cadastrado."});
                }
            }catch(Exception e){                
                Response.StatusCode = 500; // Error
                return new ObjectResult(e.Message);
            }            
        }        
    }
}