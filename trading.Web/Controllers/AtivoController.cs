using Microsoft.AspNetCore.Mvc;
using trading.Core.Interfaces;
using trading.Core.Struct;
using System;

namespace trading.Web.Controllers
{
    [Route("api/v1/ativo")]
    [ApiController]
    public class AtivoController : Controller
    {
        private readonly ConsultaAtivoInterface _service;

        public AtivoController(ConsultaAtivoInterface service){
            _service = service;
        }

        // GET api/values
        [HttpPost, Route("consulta")]
        
        public IActionResult Consulta([FromBody] Ativo _ativo)
        {
            try
            {
                var cotacao =  _service.BuscaAtivo(_ativo.Symbol);
                return Ok(new { Cotacao = cotacao });

            }catch(Exception e){
                // Erro ao tentar consultar o ativo
                Response.StatusCode = 401; // Não autorizado            
                return new ObjectResult(e.Message);
            }
        }
    }
}