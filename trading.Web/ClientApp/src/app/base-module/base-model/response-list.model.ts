import { Injectable } from '@angular/core';

@Injectable()
export class ResponseListModel {
    
    public data:any[];
    public links:any;    
    public meta:any;

}
