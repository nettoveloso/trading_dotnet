import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';


@NgModule({
    imports: [
        CommonModule,
        //RouterModule.forChild(EmpresasRoutes),
        FormsModule,
        ReactiveFormsModule,
    ],
    declarations: [
    ],
    entryComponents: [
    ],
    providers: [],
    exports: [
    ]
})

export class BaseModule {
}
