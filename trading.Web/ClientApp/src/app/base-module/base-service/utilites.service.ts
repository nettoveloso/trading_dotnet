import {Injectable} from '@angular/core';

@Injectable()
export class UtilitesService {

    public urlPrefix = "https://localhost:5001/api/v1/";
    public server = "https://localhost:5001/";

    constructor() {
    }


    /**
     * Converte objeto JSON em String no formato de parâmetro http para requisição GET
     * @param data
     * @returns {any}
     */
    formurlencoded(data: any) {
        const opts = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};

        let sorted = Boolean(opts.sorted),
            skipIndex = Boolean(opts.skipIndex),
            ignorenull = Boolean(opts.ignorenull),
            encode = function encode(value) {
                return String(value).replace(/(?:[\0-\x1F"-&\+-\}\x7F-\uD7FF\uE000-\uFFFF]|[\uD800-\uDBFF][\uDC00-\uDFFF]|[\uD800-\uDBFF](?![\uDC00-\uDFFF])|(?:[^\uD800-\uDBFF]|^)[\uDC00-\uDFFF])/g, encodeURIComponent).replace(/ /g, '+').replace(/[!'()~\*]/g,
                    function (ch: any) {
                        return '%' + ch.charCodeAt().toString(16).slice(-2).toUpperCase();
                    });
            },
            keys = function keys(obj) {
                const keyarr = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : Object.keys(obj);
                return sorted ? keyarr.sort() : keyarr;
            },
            filterjoin = function filterjoin(arr) {
                return arr.filter(function (e) {
                    return e;
                }).join('&');
            },
            objnest = function objnest(name, obj) {
                return filterjoin(keys(obj).map(function (key) {
                    return nest(name + '[' + key + ']', obj[key]);
                }));
            },
            arrnest = function arrnest(name, arr) {
                return arr.length ? filterjoin(arr.map(function (elem, index) {
                    return skipIndex ? nest(name + '[]', elem) : nest(name + '[' + index + ']', elem);
                })) : encode(name + '[]');
            },
            nest = function nest(name, value) {
                const type = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : typeof value === 'undefined' ? 'undefined' : typeof(value);
                let f = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : null;

                if (value === f) f = ignorenull ? f : encode(name) + '=' + f; else if (/string|number|boolean/.test(type)) f = encode(name) + '=' + encode(value); else if (Array.isArray(value)) f = arrnest(name, value); else if (type === 'object') f = objnest(name, value);

                return f;
            };

        return data && filterjoin(keys(data).map(function (key) {
            return nest(key, data[key]);
        }));
    }

    public timeDiff(data1, data2) {
        var d1 = new Date(data1).getTime();
        var d2 = new Date(data2).getTime();
        var df = Math.abs(d1 - d2);
        var td = {
            d: Math.round(df / (24 * 60 * 60 * 1000)), //dias
            h: Math.round(df / (60 * 60 * 1000)), //horas
            m: Math.abs(Math.round(df / (60 * 1000)) - (60 * 1000)), //minutos
            s: Math.abs(Math.round(df / 1000) - 1000)
        };
        var result = '';
        td.d > 0 ? result += td.d + ' dias ' : '';
        td.h > 0 ? result += ('0' + td.h).slice(-2) + ':' : '00:';
        td.m > 0 ? result += ('0' + td.m).slice(-2) + ':' : '00:';
        td.s > 0 ? result += ('0' + td.s).slice(-2) : '00';
        return result;
    }

    public setOnline(online:boolean){
        window.localStorage.setItem('online', JSON.stringify(online));
    }

    public getOnline(){
        return window.localStorage.getItem('online');
    }

    /**
     * Dia, atual, da semana
     */
    public diaDaSemana() {
        var semana = ["Domingo", "Segunda-Feira", "Terça-Feira", "Quarta-Feira", "Quinta-Feira", "Sexta-Feira", "Sábado"];

        var teste = new Date();
        var dia = teste.getDay();
        return semana[dia];
    }

    /**
     * Hora atual
     */
    public hora() {
        var horas = new Date().getHours();
        var horasStr: String = ("00" + horas).slice(-2);

        var minutos = new Date().getMinutes();
        var minutosStr: String = ("00" + minutos).slice(-2);

        return horasStr + ":" + minutosStr;
    }

    /**
     * Mes atual
     */
    public mesAtual() {
        var data = new Date();
        var mes = data.getMonth() + 1;
        var mesStr: String = "";
        switch (mes) {
            case 1:
                mesStr = "Janeiro";
                break;
            case 2:
                mesStr = "Fevereiro";
                break;
            case 3:
                mesStr = "Março";
                break;
            case 4:
                mesStr = "Abril";
                break;
            case 5:
                mesStr = "Maio";
                break;
            case 6:
                mesStr = "Junho";
                break;
            case 7:
                mesStr = "Julho";
                break;
            case 8:
                mesStr = "Agosto";
                break;
            case 9:
                mesStr = "Setembro";
                break;
            case 10:
                mesStr = "Outubro";
                break;
            case 11:
                mesStr = "Novembro";
                break;

            default:
                mesStr = "Dezembro";
                break;
        }

        return mesStr;
    }

    /**
     * Data de hoje
     */
    public dataHoje() {
        var data = new Date();
        var ano = data.getFullYear();
        var dia = data.getDate();
        var diaStr: String = ("00" + dia).slice(-2);

        var mes = data.getMonth() + 1;
        var mesStr: String = ("00" + mes).slice(-2);

        var horas = new Date().getHours();
        var horasStr: String = ("00" + horas).slice(-2);

        var minutos = new Date().getMinutes();
        var minutosStr: String = ("00" + minutos).slice(-2);

        var result = diaStr + "/" + mesStr + "/" + ano + " - " + horasStr + "h" + minutosStr;
        return result;
    }

    public getDataDmy(){
        var data = new Date();
        var ano = data.getFullYear();
        var dia = data.getDate();
        var diaStr: String = ("00" + dia).slice(-2);

        var mes = data.getMonth() + 1;
        var mesStr: String = ("00" + mes).slice(-2);

        var result = diaStr + "/" + mesStr + "/" + ano;
        return result;
    }

    /**
     * Data por extenso
     */
    public dataPorExtenso() {

        var data = new Date();
        var ano = data.getFullYear();
        var dia = data.getDate();

        return dia + " de " + this.mesAtual() + " de " + ano;
    }

    /**
     * Converter string em Date dd/mm/yyyy
     * @param {string} dateString
     * @returns {Date}
     */
    public convertStringToDate(dateString:string){
        var dateParts = dateString.split("/");
        var dateObject = new Date(parseInt(dateParts[2]), parseInt(dateParts[1]) - 1, parseInt(dateParts[0])); // month is 0-based
        return dateObject;
    }

    /**
     * Converter string em Date yyyy-mm-dd hh:mm:ss
     * @param {string} dateString
     * @returns {Date}
     */
    public convertToDate(dateString:string){
        var dateObject = new Date(dateString); // month is 0-based
        return dateObject;
    }

}
