import { Injectable }       from '@angular/core';
import { Http, Headers }    from '@angular/http';
import { Observable }       from 'rxjs/Observable';
import {HttpHeaders}        from "@angular/common/http";
import 'rxjs/add/operator/map';

//Services
import {UtilitesService} from './utilites.service';

//Models
import {AuthModel}  from '../../authentication/models/auth.model';

@Injectable()
export class AuthService {

    constructor(private _http:Http, public utilitesService: UtilitesService){
    }

    post(data):Observable<AuthModel>{
        var headers = new Headers();
        headers.append('Content-Type','application/json; charset=utf-8');

        return this._http.post(this.utilitesService.urlPrefix+"auth/login",JSON.stringify(data),{headers:headers}).map(res=>res.json());
    }

    setUser(user){
        window.localStorage.setItem('user', JSON.stringify(user));
    }

    //return token
    getUser(){
        //return JSON.stringify(window.localStorage.getItem('user'));
        return window.localStorage.getItem('user');
    }

    setToken(token){
        window.localStorage.setItem('token', token);
    }

    //return token
    getToken(){
        return window.localStorage.getItem('token');
    }

    public isAuth(){
        var user = this.getUser();
        if(user == null || user == undefined || user == 'null' || user == 'undefined'){
        return false;
        }

        return true;
    }

    logout(){
        window.localStorage.removeItem('user');
        window.localStorage.removeItem('token');
        window.localStorage.removeItem('company');
        window.localStorage.removeItem('empresa');
    }

    getAuthHeaders(){
        return new HttpHeaders({
            'Content-Type':'application/json; charset=utf-8',
            'Accept':'application/json',
            'Authorization':'Bearer '+this.getToken(),
            "Access-Control-Allow-Origin": "*"
        });
    }

    isAuthenticated(){
        let user:any = this.getUser();
        if(user == null || user == ""){
            return false;
        }
        return true;
    }
}
