//Services
import {UtilitesService} from '../../base-module/base-service/utilites.service';

import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import { Observable }       from 'rxjs/Observable';

@Injectable()
export class UsuarioService {

    constructor(private _http: HttpClient, public utilitesService: UtilitesService) {
    }

    /**
     * Cadastra usuário e empresa
     * @param data
     * @returns {Observable<Object>}
     */
    public create(data): Observable<any> {
        return this._http.post(this.utilitesService.urlPrefix+"usuario/novo",data);
    }

}
