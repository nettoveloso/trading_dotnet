import { Injectable } from '@angular/core';

@Injectable()
export class AuthModel {

    public email:string;
    public password:string;
    public token:string;
    public data:any;
    public user:any;
}
