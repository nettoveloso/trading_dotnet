import { Injectable } from '@angular/core';

@Injectable()
export class ResponseModel {
    public current_page:string;
    public data:any[];
    public media:any[];    
    public from:string;
    public last_page:string;
    public next_page_url:string;
    public path:string;
    public per_page:string;
    public prev_page_url:string;
    public to:string;
    public total:string;
    public error:string;
    public category:any[];
    public magazine:any[];
}
