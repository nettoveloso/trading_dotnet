import { Component, SecurityContext, ViewEncapsulation  }    from '@angular/core';
import { Router }       from "@angular/router";
import { FormControl, Validators } from '@angular/forms';
import {AuthService} from "../../base-module/base-service/auth.service";
import {UsuarioService} from '../../authentication/services/usuario.service';
import { DomSanitizer } from '@angular/platform-browser';
import { AlertConfig } from 'ngx-bootstrap/alert';

export function getAlertConfig(): AlertConfig {
    return Object.assign(new AlertConfig(), { type: 'success' });
  }


@Component({
  selector: 'app-dashboard',
  templateUrl: 'register.component.html'
})
export class RegisterComponent {

    name                = new FormControl('', [Validators.required, Validators.maxLength(255)]);
    password            = new FormControl('', [Validators.required, Validators.maxLength(255)]);
    confirm_password    = new FormControl('', [Validators.required, Validators.maxLength(255)]);
    email               = new FormControl('', [Validators.required,Validators.email, Validators.maxLength(255)]);

    model:any = {
        Name:"",
        Email:"",
        Password:"",
        confirm_password:"",
    };

    constructor(
        private route: Router,
        public service: UsuarioService,
        public authService:AuthService) {

     }

    getErrorNameMessage() {
        return this.name.hasError('required') ? 'Nome é um campo obrigatório' : '';
    }

    getErrorEmailMessage() {
        return this.email.hasError('required') ? 'E-mail é um campo obrigatório' : '';
    }

    isInvalidForm()
    {
        if (!this.name.valid){
            return true;
        }
        if (!this.email.valid) {
            return true;
        }

        if(this.password.value != this.confirm_password.value){
            return true;
        }

        return false;
    }

    voltar()
    {
        this.route.navigate(["/login"]);
    }

    salvar()
    {
        this.model.Name     = this.name.value;
        this.model.Email    = this.email.value;

        //envia password e confirmação se existir
        if(this.password != null && this.confirm_password != null){
            this.model.Password = this.password.value;
            this.model.confirm_password = this.confirm_password.value;
        }

        this.service.create(this.model).subscribe(
            data => {
                console.log(data);
                alert(data.msg);
                this.route.navigate(["/login"]);
            },
            error => {
                console.log(error.error);
                alert(error.error.message);
            },
            () => {
            }
        );

    }

}
