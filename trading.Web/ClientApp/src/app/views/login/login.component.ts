import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { LoginModel } from '../../authentication/models/login.model';
import { AuthService } from '../../base-module/base-service/auth.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: 'login.component.html'
})

export class LoginComponent implements OnInit{

    private model = new LoginModel;
    public form: FormGroup;
    constructor(private fb: FormBuilder, private router: Router, private service: AuthService) {
        service.logout();
    }

    ngOnInit() {
        this.form = this.fb.group (
            {
                email: [null , Validators.compose ( [ Validators.required ] )] ,
                password: [null , Validators.compose ( [ Validators.required ] )]
            }
        );
    }

    onSubmit() {

        if (this.form.value.email == null || this.form.value.email == ""){
          alert("Campo email é obrigatório");
          return ;
        }

        if (this.form.value.password == null || this.form.value.password == "") {
          alert("Campo senha é obrigatório");
          return;
        }

        this.model.email = this.form.value.email;
        this.model.password = this.form.value.password;

        this.service.post(this.model)
          .subscribe(
            data => {
                console.log(data);
                this.service.setUser(data.user);
                this.service.setToken(data.token);
                this.router.navigate(['/dashboard']);
            },
            error => {
                console.log((JSON.parse(error._body)));
                alert(JSON.parse(error._body).message);
            },
            () => {
              console.log("Complete");
            }
        );

    }

    cadastro(){
        this.router.navigate(['/register']);
    }



}


