import { Component, OnInit } from '@angular/core';
import { getStyle, hexToRgba } from '@coreui/coreui/dist/js/coreui-utilities';
import { CustomTooltips } from '@coreui/coreui-plugin-chartjs-custom-tooltips';
import { AuthService} from "../../base-module/base-service/auth.service";
import {Router} from "@angular/router";
@Component({
  templateUrl: 'dashboard.component.html'
})
export class DashboardComponent implements OnInit {

    constructor(public _authService:AuthService,public route: Router ){
        this.canActivate();
    }

    ngOnInit(): void {
    }

    canActivate(): void {
        if (!this._authService.isAuthenticated()) {
            alert("usuário não autenticado");
            this.route.navigate(['/login']);
        }
    }
}
