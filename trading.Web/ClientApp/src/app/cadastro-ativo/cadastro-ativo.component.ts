import { Component } from '@angular/core';
import { AtivoService } from '../service/ativo.service';

@Component({
  templateUrl: 'cadastro-ativo.component.html',
  styleUrls: ['cadastro-ativo.component.css']
})

export class CadastroAtivoComponent {

    keyword = 'ativo';
    data = [
    ];

    params: any = {
        Symbol:null,
    };

    constructor(public service: AtivoService,) { }

    buscaAtivo(keyword){
        console.log('aqui');
        this.params.Symbol = keyword;
        this.service.getAll(this.params).subscribe(
            data => {
                this.data = data.cotacao.content
            	console.log(data.cotacao.content);
            },
            error => {
                console.log("error: " + error);

            },
            () => {
                console.log("complete");
            }
        );
    }

    selectEvent(item) {
        console.log(item);
        // do something with selected item
    }

    onChangeSearch(val: string) {
        console.log(val);
        if(val.length >= 3){
            this.buscaAtivo(val);
        }
        // fetch remote data from here
        // And reassign the 'data' which is binded to 'data' property.
    }

    onFocused(e){
        console.log('focus');
    // do something when input is focused
    }



}
