import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  templateUrl: 'ativo.component.html'
})
export class AtivoComponent {

  constructor(private route: Router,) { }

  novo(){
    this.route.navigate(["/ativo/cadastro"]);
  }


}
